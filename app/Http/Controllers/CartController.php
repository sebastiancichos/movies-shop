<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;

class CartController extends Controller
{
	public function store(Request $request, $id)
	{
		$Products = $request->session()->exists('cart') ? $request->session()->get('cart')['products'] : [];
		$Product = Movie::findOrFail($id);
		array_push($Products, $Product);

		$Total = 0;
		foreach ($Products as $Item) {
			$Total += $Item['price'];
		}

		$Cart['total'] = $Total;
		$Cart['products'] = $Products;

		$request->session()->put('cart', $Cart);

		return view('cart.show');
	}

	public function destroy(Request $request, $index)
	{
		$Products = $request->session()->get('cart')['products'];
		array_splice($Products, $index, 1);

		if (count($Products) > 0) {
			$Total = 0;
			foreach ($Products as $Item) {
				$Total += $Item['price'];
			}

			$Cart['total'] = $Total;
			$Cart['products'] = $Products;

			$request->session()->put('cart', $Cart);
		} else {
			$request->session()->forget('cart');
		}

		return view('cart.show');
	}
}
