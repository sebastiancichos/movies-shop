<?php

namespace App\Http\Controllers;

use App\Category;
use App\Movie;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$movies = Movie::paginate(15);
    	$categories = Category::all();

        return view('home', compact(['movies', 'categories']));
    }
}
