<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
	protected $table = 'movies';

	protected $fillable = [
		'title', 'details', 'year', 'price', 'category'
	];

	public function category() {
		return $this->belongsTo(Category::class);
	}
}
