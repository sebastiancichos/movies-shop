<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Movie::class, function (Faker $faker) {
	return [
		'title' => $faker->unique()->sentence(random_int(3, 5), true),
		'description' => $faker->paragraphs(5, true),
		'price' => $faker->numberBetween(2200, 6000),
		'year' => $faker->numberBetween(1970, 2017)
	];
});
