<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// $this->call(UsersTableSeeder::class);

	    $categories = array('Akcji', 'Przygodowy', 'Familijny', 'Horror', 'Parodia', 'Fantastyka', 'Dokumentalny', 'Melodramat', 'Obcyczajowy');

	    foreach ($categories as $category) {
		    factory(App\Category::class)->create([
			    'name' => $category,
		    ])->each(function ($u) {
			    $u->movies()->saveMany(factory(App\Movie::class, random_int(2, 6))->make());
		    });
	    }

    }
}
