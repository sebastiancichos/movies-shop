<div class="panel-heading">Koszyk</div>
@if (session('cart'))
    <div class="panel-body">
        @foreach(Session::get('cart')['products'] as $movie)
            <div class="media">
                <div class="media-left">
                    <a href="{{ route('movie.show', ['id' => $movie->id]) }}">
                        <img class="media-object" src="https://picsum.photos/150/120/?gravity=center&image={{ $movie->id }}" alt="{{ $movie->name }}">
                    </a>
                </div>
                <div class="media-body">
                    <h5 class="media-heading">
                        {{ $movie->title }}<br><br>
                        <span class="label label-default">{{ $movie->category->name }}</span>
                    </h5>
                    <h5>Cena: {{ number_format(($movie->price/100), 2) }} zł</h5>
                    <button data-url="{{ route('cart.destroy', ['index' => $loop->index]) }}" type="button" class="deleteFromCart btn btn-xs btn-danger">Usuń</button>
                </div>
            </div>
        @endforeach
    </div>
    <div class="panel-footer">
        Suma do zapłaty: {{ number_format((Session::get('cart')['total']/100), 2) }} zł
    </div>
@else
    <div class="panel-body">
        Brak produktów w koszyku
    </div>
@endif