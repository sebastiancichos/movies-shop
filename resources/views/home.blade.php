@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Katalog
                        <div class="pull-right">
                            <select onchange="if (this.value) window.location.href=this.value">
                                <option value="{{ route('home') }}">
                                    @if (!empty($categoryId))
                                        Wszystkie kategorie
                                    @else
                                        Wybierz kategorię
                                    @endif
                                </option>
                                @foreach ($categories as $category)
                                    <option
                                            @if (!empty($categoryId) && $categoryId == $category->id))
                                            selected
                                            @endif
                                            value="{{ route('category', ['id' => $category->id]) }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="panel-body" style="display: flex; flex-wrap: wrap; padding-top: 30px;">
                        @if (count($movies) === 0)
                            Brak filmów
                        @else
                            @foreach ($movies as $movie)
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="media-body">
                                        <a href="{{ route('movie.show', ['id' => $movie->id]) }}">
                                            <img class="media-object img-responsive" src="https://picsum.photos/200/160/?gravity=center&image={{ $movie->id }}" alt="{{ $movie->name }}">
                                        </a>
                                        <h4 class="media-heading" style="margin-top: 15px; margin-bottom: 10px;">
                                            {{ $movie->title }}
                                        </h4>
                                        <button style="margin-bottom: 10px;" data-url="{{ route('cart.store', ['id' => $movie->id]) }}" type="button" class="addToCart btn btn-block btn-default btn-large btn-success pull-right">Dodaj do koszyka</button>
                                    </div>
                                </div>
                            @endforeach
                            {{ $movies->links() }}
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="cart" class="panel panel-default">
                    @include('cart.show')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            function bind() {
                $('#addToCart, .addToCart').unbind('click');
                $('#addToCart, .addToCart').on('click', function () {
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        success: function(data) {
                            $('#cart').html(data);
                            bind();
                        }
                    });
                });
                $('.deleteFromCart').on('click', function () {
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        success: function(data) {
                            $('#cart').html(data);
                            bind();
                        }
                    });
                });
            }
            bind();
        });
    </script>
@endsection
