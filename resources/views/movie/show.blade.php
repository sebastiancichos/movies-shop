@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ url()->previous() }}" class="btn btn-warning btn-large btn-success">Wróć do listy</a>
                        <button id="addToCart" data-url="{{ route('cart.store', ['id' => $movie->id]) }}" type="button" class="btn btn-default btn-large btn-success pull-right">Dodaj do koszyka</button>
                    </div>
                    <div class="panel-body">
                        <h1>{{ $movie->title }}</h1>
                        <br>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h3 style="margin-top: 0px; margin-bottom: 0px;">Kategoria
                                    <span class="label label-default pull-right">{{ $movie->category->name }}</span>
                                </h3>
                            </li>
                            <li class="list-group-item">
                                <h3 style="margin-top: 0px; margin-bottom: 0px;">Rok produkcji
                                    <span class="label label-default pull-right">{{ $movie->year }}</span>
                                </h3>
                            </li>
                            <li class="list-group-item">
                                <h3 style="margin-top: 0px; margin-bottom: 0px;">Cena
                                    <span class="label label-default pull-right">{{ number_format(($movie->price/100), 2) }} zł</span>
                                </h3>
                            </li>
                        </ul>
                        <p>
                            {{ $movie->description }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="cart" class="panel panel-default">
                    @include('cart.show')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function() {
            function bind() {
                $('#addToCart, .addToCart').unbind('click');
                $('#addToCart, .addToCart').on('click', function () {
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        success: function(data) {
                            $('#cart').html(data);
                            bind();
                        }
                    });
                });
                $('.deleteFromCart').on('click', function () {
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        success: function(data) {
                            $('#cart').html(data);
                            bind();
                        }
                    });
                });
            }
            bind();
        });
    </script>
@endsection
