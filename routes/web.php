<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/category/{id}', 'CategoryController@show')->name('category');
Route::get('/movie/{id}', 'MovieController@show')->name('movie.show');
Route::get('/cart/add/{id}', 'CartController@store')->name('cart.store');
Route::get('/cart/delete/{index}', 'CartController@destroy')->name('cart.destroy');

Auth::routes();
